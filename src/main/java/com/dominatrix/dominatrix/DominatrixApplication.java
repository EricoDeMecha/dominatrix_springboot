package com.dominatrix.dominatrix;

import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DominatrixApplication implements CommandLineRunner {
	private static org.slf4j.Logger log = LoggerFactory.getLogger(DominatrixApplication.class);
	public static void main(String[] args) {
		log.info("STARTING APPLICATION");
		SpringApplication.run(DominatrixApplication.class, args);
		log.info("ENDING APPLICATION");
	}
	@Override
    public void run(String... args) {
        log.info("EXECUTING : command line runner");
 
        for (int i = 0; i < args.length; ++i) {
            log.info("args[{}]: {}", i, args[i]);
        }
    }
}
